package com.example.santoshmegamartadmin.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.example.santoshmegamartadmin.R
import com.example.santoshmegamartadmin.databinding.FragmentSliderBinding
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.util.*


class SliderFragment : Fragment() {

    private lateinit var binding:FragmentSliderBinding
    private var imageURI:Uri?= null
    private lateinit var dialog:Dialog

    //checking for request for opening gallery
    private var launchGalleryActivity =registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ){
        if (it.resultCode== Activity.RESULT_OK){
            imageURI = it.data!!.data
            binding.imageView.setImageURI(imageURI)
        }
        else
            Toast.makeText(context, "No Image Selected", Toast.LENGTH_SHORT).show()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentSliderBinding.inflate(inflater)

        dialog=Dialog(requireContext()).apply {
            setContentView(R.layout.progress_layout)
            setCancelable(false)

        }

        binding.apply {
            imageView.setOnClickListener {
                val intent = Intent("android.intent.action.GET_CONTENT")
                intent.type="image/*"
                launchGalleryActivity.launch(intent)

            }
            btnSelectProductFromGallery.setOnClickListener {
                if(imageURI !=null){
                    //upload the image to the firebase
                    uploadImage(imageURI!!)
                }else{
                    Toast.makeText(context, "Please select an image to upload", Toast.LENGTH_SHORT).show()
                }
            }

        }
        return binding.root
    }

    private fun uploadImage(uri: Uri) {

        dialog.show()

        val fileName = UUID.randomUUID().toString()

        val refStorage = FirebaseStorage.getInstance().reference.child("slider/$fileName")
        refStorage.putFile(uri)
            .addOnSuccessListener { image ->
                image.storage.downloadUrl.addOnSuccessListener { 
                    storeData(image.toString())
                }
            }
            .addOnFailureListener{
                dialog.dismiss()
                Toast.makeText(context, "Something went wrong with Storage", Toast.LENGTH_SHORT).show()
            }

    }

    private fun storeData( image: String) {
        val db= Firebase.firestore

        val data = hashMapOf<String,Any>(
            "img" to image
        )
        db.collection("slider").document("item").set(data)
            .addOnSuccessListener {
                dialog.dismiss()
                Toast.makeText(context, "Slider Uploaded", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                dialog.dismiss()
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show()
            }

    }

}